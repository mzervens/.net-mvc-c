﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace md2.Models
{
    public class AuthorModel
    {
        private Rand r = new Rand();

        public virtual List<authors> GetPage(int offset, int limit)
        {
            List<authors> authors = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var auths = from p in ctx.authors.OrderBy(x => x.au_fname + x.au_lname)
                                         .Skip(offset).Take(limit)
                               select p;

                    authors = auths.ToList<authors>();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return authors;
        }

        public virtual List<authors> GetAllAuthors()
        {
            List<authors> authors = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var auths = from p in ctx.authors.OrderBy(x => x.au_fname + x.au_lname)
                                select p;

                    authors = auths.ToList<authors>();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return authors;
        }

        public virtual List<authors> GetElement(string id)
        {
            List<authors> author = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var auth = from p in ctx.authors.Where(x => x.au_id == id)
                              select p;

                    author = auth.ToList<authors>();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return author;

        }

        public List<authors> GetElementWithTitles(string id)
        {
            List<authors> author = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var auth = from p in ctx.authors.Include("titleauthor").Where(x => x.au_id == id)
                               select p;

                    author = auth.ToList<authors>();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return author;

        }

        public virtual authors DeleteElement(string id)
        {
            authors deletedElement = null;

            try
            {
                List<authors> element = GetElementWithTitles(id);
                if (element.Count != 0)
                {
                    authors author = element[0];
                    if (author.titleauthor.Count > 0)
                    {
                        throw new InvalidOperationException();
                    }
                    using (pubs4Entities ctx = new pubs4Entities())
                    {
                        ctx.authors.Attach(author);
                        ctx.authors.Remove(author);
                        ctx.SaveChanges();
                        deletedElement = author;
                    }
                }
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return deletedElement;


        }

        public virtual string CreateElement(authors author)
        {
            string id = "";

            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    while (true)
                    {
                        id = r.GetRandomString(3, true) + "-" + r.GetRandomString(2, true) + "-" + r.GetRandomString(4, true);
                        var existsId = from p in ctx.authors.Where(x => x.au_id == id)
                                       select p;

                        if (existsId.ToList<authors>().Count == 0)
                        {
                            break;
                        }
                    }

                    author.au_id = id;
                    author.contract = false;
                    author.phone = "123456789012";

                    ctx.authors.Add(author);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return id;
        }

        public virtual void SaveModified(authors author)
        {
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    author.contract = false;
                    author.phone = "123456789012";

                    ctx.authors.Attach(author);
                    ctx.Entry(author).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}