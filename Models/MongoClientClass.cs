﻿using MongoDB.Driver;
using MongoDB.Bson;


class MongoClientClass
{
    private string mConnectionStr = "mongodb://localhost:27017";
    public MongoClient mClient = new MongoClient();
    public IMongoDatabase mDB = null;

    public MongoClientClass(string pConnectionStr, string pDB) : this(pDB)
    {
        mConnectionStr = pConnectionStr;

    }

    public MongoClientClass(string pDB)
    {
        mClient = new MongoClient(mConnectionStr);
        mDB = mClient.GetDatabase(pDB);
    }


}