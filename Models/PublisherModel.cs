﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace md2.Models
{
    public class PublisherModel
    {
        private Rand r = new Rand();

        public virtual List<publishers> GetPage(int offset, int limit)
        {
            List<publishers> publishers = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                     var pubs = from p in ctx.publishers.OrderBy(x => x.pub_name).Skip(offset).Take(limit)
                                     select p;

                    publishers = pubs.ToList<publishers>();
                }
            }
            catch(Exception)
            {
                throw;
            }

            return publishers;
        }

        public virtual List<publishers> GetAllPublishers()
        {
            List<publishers> publishers = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var pubs = from p in ctx.publishers.OrderBy(x => x.pub_name)
                               select p;

                    publishers = pubs.ToList<publishers>();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return publishers;
        }

        public virtual List<publishers> GetElement(string id)
        {
            List<publishers> publisher = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var pub = from p in ctx.publishers.Where(x => x.pub_id == id)
                                    select p;

                    publisher = pub.ToList<publishers>();
                }
            }
            catch(Exception)
            {
                throw;
            }
            return publisher;

        }

        public List<publishers> GetElementWithTitles(string id)
        {
            List<publishers> publisher = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var pub = from p in ctx.publishers.Include("titles").Where(x => x.pub_id == id)
                              select p;

                    publisher = pub.ToList<publishers>();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return publisher;

        }

        public virtual publishers DeleteElement(string id)
        {
            publishers deletedElement = null;

            try
            {
                List<publishers> element = GetElementWithTitles(id);
                if (element.Count != 0)
                {
                    publishers publisher = element[0];
                    if(publisher.titles.Count > 0)
                    {
                        throw new InvalidOperationException();
                    }
                    using (pubs4Entities ctx = new pubs4Entities())
                    {
                        ctx.publishers.Attach(publisher);
                        ctx.publishers.Remove(publisher);
                        ctx.SaveChanges();
                        deletedElement = publisher;
                    }
                }
            }
            catch(InvalidOperationException)
            {
                throw;
            }
            catch(Exception)
            {
                throw;
            }

            return deletedElement;
        }


        public virtual string CreateElement(publishers publisher)
        {
            string id = "";

            try
            {
                using(pubs4Entities ctx = new pubs4Entities())
                {
                    while(true)
                    {
                        id = "99" + r.GetRandomString(2, true);
                        var existsId = from p in ctx.publishers.Where(x => x.pub_id == id)
                                       select p;

                        if(existsId.ToList<publishers>().Count == 0)
                        {
                            break;
                        }
                    }

                    publisher.pub_id = id;

                    ctx.publishers.Add(publisher);
                    ctx.SaveChanges();
                }
            }
            catch(Exception)
            {
                throw;
            }

            return id;
        }


        public virtual void SaveModified(publishers publisher)
        {
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    ctx.publishers.Attach(publisher);
                    ctx.Entry(publisher).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}