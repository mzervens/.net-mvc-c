﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using MongoDB.Bson;

namespace md2.Models
{
    public class AuthorMongoModel : AuthorModel
    {
        MongoClientClass cc = new MongoClientClass("pubs4");

        public override List<authors> GetPage(int offset, int limit)
        {
            List<authors> authors = null;

            var collection = cc.mDB.GetCollection<authors>("authors");

            authors = collection.Find(new BsonDocument())
                        .Sort("{au_fname: 1, au_lname: 1}")
                        .Skip(offset)
                        .Limit(limit).ToList<authors>();

            return authors;
        }

        public override List<authors> GetElement(string id)
        {
            var collection = cc.mDB.GetCollection<authors>("authors");

            var auth = collection.Find(new BsonDocument("_id", ObjectId.Parse(id)))
                            .ToList<authors>();

            return auth;

        }

        public override authors DeleteElement(string id)
        {
            authors deletedElement = null;
            var collection = cc.mDB.GetCollection<authors>("authors");
            var titleCol = cc.mDB.GetCollection<BsonDocument>("titles");

            var auth = this.GetElement(id);

            var usedInBooks = titleCol.Find(new BsonDocument("authors", ObjectId.Parse(id))).ToList<BsonDocument>();
            if (usedInBooks.Count > 0)
            {
                throw new InvalidOperationException();
            }

            collection.FindOneAndDelete(new BsonDocument("_id", ObjectId.Parse(id)));

            if (auth.Count == 1)
            {
                return auth[0];
            }
            return deletedElement;

        }

        public override string CreateElement(authors auth)
        {
            string id = "";
            var collection = cc.mDB.GetCollection<BsonDocument>("authors");

            BsonDocument authDoc = new BsonDocument();


            authDoc.Add(new BsonElement("au_lname", auth.au_lname));
            authDoc.Add(new BsonElement("au_fname", auth.au_fname));
            authDoc.Add(new BsonElement("address", auth.address));
            authDoc.Add(new BsonElement("updated", new BsonDateTime(DateTime.Now)));

            collection.InsertOne(authDoc);

            id = authDoc["_id"].ToString();

            return id;
        }

        public List<authors> GetAuthorsByUpdatedDates(DateTime from, DateTime to)
        {
            var collection = cc.mDB.GetCollection<authors>("authors");
            List<authors> authors = null;

            authors = collection.Find(x => x.updated >= from & x.updated <= to).ToList();
            return authors;
        }

        public override void SaveModified(authors author)
        {
            var collection = cc.mDB.GetCollection<BsonDocument>("authors");
            var update = Builders<BsonDocument>.Update
                .Set("au_lname", author.au_lname)
                .Set("au_fname", author.au_fname)
                .Set("address", (BsonValue)author.address ?? BsonNull.Value)
                .Set("updated", new BsonDateTime(DateTime.Now));

            var filter = new BsonDocument("_id", author._id);

            collection.UpdateOne(filter, update);
        }


        public override List<authors> GetAllAuthors()
        {
            List<authors> authors = null;
            var collection = cc.mDB.GetCollection<authors>("authors");

            authors = collection.Find(new BsonDocument())
                        .Sort("{au_fname: 1, au_lname: 1}").ToList<authors>();

            return authors;
        }
    

    }

}
