﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MongoDB.Driver;
using MongoDB.Bson;

namespace md2.Models
{
    public class PublisherMongoModel : PublisherModel
    {
        MongoClientClass cc = new MongoClientClass("pubs4");


        public override List<publishers> GetPage(int offset, int limit)
        {
            List<publishers> publishers = null;

            var collection = cc.mDB.GetCollection<publishers>("publishers");

            publishers = collection.Find(new BsonDocument())
                    .Skip(offset)
                    .Limit(limit)
                    .Sort("{pub_name: 1}").ToList<publishers>();

            return publishers;
        }

        public override List<publishers> GetElement(string id)
        {
            var collection = cc.mDB.GetCollection<publishers>("publishers");

            var pub = collection.Find(new BsonDocument("_id", ObjectId.Parse(id)))
                            .ToList<publishers>();

            return pub;

        }

        public override publishers DeleteElement(string id)
        {
            publishers deletedElement = null;
            var collection = cc.mDB.GetCollection<publishers>("publishers");
            var titleCol = cc.mDB.GetCollection<BsonDocument>("titles");

            var pub = this.GetElement(id);

            var usedInBooks = titleCol.Find(new BsonDocument("publisher", ObjectId.Parse(id))).ToList<BsonDocument>();
            if(usedInBooks.Count > 0)
            {
                throw new InvalidOperationException();
            }

            collection.FindOneAndDelete(new BsonDocument("_id", ObjectId.Parse(id)));

            if (pub.Count == 1)
            {
                return pub[0];
            }
            return deletedElement;

        }



        public override string CreateElement(publishers publisher)
        {
            string id = "";
            var collection = cc.mDB.GetCollection<BsonDocument>("publishers");

            BsonDocument pubDoc = new BsonDocument();
            pubDoc.Add(new BsonElement("pub_name", publisher.pub_name));
            pubDoc.Add(new BsonElement("city", publisher.city));
            pubDoc.Add(new BsonElement("state", (BsonValue)publisher.state ?? BsonNull.Value));
            pubDoc.Add(new BsonElement("country", publisher.country));

            collection.InsertOne(pubDoc);

            id = pubDoc["_id"].ToString();


            return id;
        }

        public override void SaveModified(publishers publisher)
        {
            var collection = cc.mDB.GetCollection<BsonDocument>("publishers");
            var update = Builders<BsonDocument>.Update
                .Set("pub_name", publisher.pub_name)
                .Set("city", publisher.city)
                .Set("state", (BsonValue)publisher.state ?? BsonNull.Value)
                .Set("country", publisher.country);

            var filter = new BsonDocument("_id", publisher._id);

            collection.UpdateOne(filter, update);
        }

        public override List<publishers> GetAllPublishers()
        {
            List<publishers> publishers = null;

            var collection = cc.mDB.GetCollection<publishers>("publishers");

            publishers = collection.Find(new BsonDocument())
                    .Sort("{pub_name: 1}").ToList<publishers>();

            return publishers;
        }

    }

}