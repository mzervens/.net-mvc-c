﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using MongoDB.Bson;

using MongoDB.Driver.Linq;

namespace md2.Models
{
    public class BookMongoModel : BookModel
    {
        MongoClientClass cc = new MongoClientClass("pubs4");

        public override List<titles> GetPage(int offset, int limit)
        {
            var collection = cc.mDB.GetCollection<titles>("titles");
            List<titles> page = null;

            page = collection.Find(new BsonDocument())
            .Sort("{title: 1}")
            .Skip(offset)
            .Limit(limit).ToList<titles>();

            return page;
        }

        public override List<titles> GetAllBooks()
        {
            var collection = cc.mDB.GetCollection<titles>("titles");
            List<titles> page = null;

            page = collection.Find(new BsonDocument())
            .Sort("{title: 1}").ToList<titles>();

            return page;
        }

        public List<titles> GetBooksByUpdatedDates(DateTime from, DateTime to)
        {
            var collection = cc.mDB.GetCollection<titles>("titles");
            List<titles> books = null;

            books = collection.Find(x => x.updated >= from & x.updated <= to).ToList();
            return books;
        }

        private List<titleauthor> convertAuthorLToTitleAuthorL(List<authors> auth, titles title)
        {
            List<titleauthor> tal = new List<titleauthor>();

            foreach(authors a in auth)
            {
                tal.Add(convertAuthorToTitleAuthor(a, title));
            }

            return tal;
        }

        private titleauthor convertAuthorToTitleAuthor(authors auth, titles title)
        {
            titleauthor ta = new titleauthor();
            ta.titles = title;
            ta.authors = auth;
            ta.au_id = auth._id.ToString();
            ta.title_id = title._id.ToString();

            return ta;

        }

        public override List<titles> GetElement(string id)
        {
            var collection = cc.mDB.GetCollection<titles>("titles");
            var pubCol = cc.mDB.GetCollection<publishers>("publishers");
            var authCol = cc.mDB.GetCollection<authors>("authors");

            var title = collection.Find(new BsonDocument("_id", ObjectId.Parse(id)))
                            .ToList<titles>();

            if (title.Count > 0)
            {
                var publisher = pubCol.Find(new BsonDocument("_id", title[0].publisher)).ToList<publishers>();
                if(publisher.Count > 0)
                {
                    title[0].publishers = publisher[0];
                }


                var filter = Builders<authors>.Filter.In("_id", title[0].authors);
                var auths = authCol.Find(filter).ToList<authors>();

                title[0].titleauthor = convertAuthorLToTitleAuthorL(auths, title[0]);

            }
            return title;

        }

        public override titles DeleteElement(string id)
        {
            titles deletedElement = null;
            var titleCol = cc.mDB.GetCollection<BsonDocument>("titles");

            var title = this.GetElement(id);

            titleCol.FindOneAndDelete(new BsonDocument("_id", ObjectId.Parse(id)));

            if (title.Count == 1)
            {
                return title[0];
            }
            return deletedElement;

        }


        public override string CreateElement(titles book, List<string> authorIds)
        {
            string id = "";

            var collection = cc.mDB.GetCollection<BsonDocument>("titles");

            BsonDocument titleDoc = new BsonDocument();
            titleDoc.Add(new BsonElement("title", book.title));
            titleDoc.Add(new BsonElement("pubdate", book.pubdate));
            titleDoc.Add(new BsonElement("royalty", book.royalty));
            titleDoc.Add(new BsonElement("publisher", book.publishers._id));
            titleDoc.Add(new BsonElement("updated", new BsonDateTime(DateTime.Now)));

            BsonArray titleAuths = new BsonArray();
            foreach(string authId in authorIds)
            {
                titleAuths.Add(ObjectId.Parse(authId));
            }

            titleDoc.Add(new BsonElement("authors", titleAuths));

            collection.InsertOne(titleDoc);

            id = titleDoc["_id"].ToString();

            return id;

        }


        public override void SaveModified(titles book, List<string> authorIds)
        {

            var collection = cc.mDB.GetCollection<BsonDocument>("titles");
            BsonArray titleAuths = new BsonArray();
            foreach (string authId in authorIds)
            {
                titleAuths.Add(ObjectId.Parse(authId));
            }

            var update = Builders<BsonDocument>.Update
                .Set("title", book.title)
                .Set("pubdate", book.pubdate)
                .Set("royalty", book.royalty)
                .Set("publisher", book.publishers._id)
                .Set("authors", titleAuths)
                .Set("updated", new BsonDateTime(DateTime.Now));

            var filter = new BsonDocument("_id", book._id);
            collection.UpdateOne(filter, update);

        }

    }

}
