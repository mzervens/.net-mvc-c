﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace md2.Models
{
    public class BookModel
    {
        private Rand r = new Rand();

        public virtual List<titles> GetPage(int offset, int limit)
        {
            List<titles> page = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var data = from t in ctx.titles.Include("publishers").Include("titleauthor")
                                         .OrderBy(x => x.title).Skip(offset).Take(limit)
                               select t;

                    page = data.ToList<titles>();

                }
            }
            catch (Exception)
            {
                throw;
            }

            return page;
        }

        public virtual List<titles> GetAllBooks()
        {
            List<titles> books = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var data = from t in ctx.titles.Include("publishers").Include("titleauthor")
                                         .OrderBy(x => x.title)
                               select t;

                    books = data.ToList<titles>();

                }
            }
            catch (Exception)
            {
                throw;
            }

            return books;
        }

        public virtual List<titles> GetElement(string id)
        {
            List<titles> book = null;
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    var data = from t in ctx.titles.Include("publishers").Include("titleauthor")
                                         .Where(x => x.title_id == id)
                               select t;

                    book = data.ToList<titles>();

                }
            }
            catch (Exception)
            {
                throw;
            }

            return book;
        }

        public virtual string CreateElement(titles book, List<string> authorIds)
        {
            string id = "";

            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    ctx.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
                    while (true)
                    {
                        id = r.GetRandomString(6);
                        var existsId = from p in ctx.titles.Where(x => x.title_id == id)
                                       select p;

                        if (existsId.ToList<titles>().Count == 0)
                        {
                            break;
                        }
                    }

                    book.title_id = id;
                    book.type = "UNDECIDED";

                    foreach(string authorId in authorIds)
                    {
                        titleauthor ta = new titleauthor();
                        ta.au_id = authorId;
                        ta.title_id = id;

                        book.titleauthor.Add(ta);
                    }

                    ctx.publishers.Attach(book.publishers);
                    ctx.titles.Add(book);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return id;

        }

        public virtual titles DeleteElement(string id)
        {
            titles deletedElement = null;

            try
            {
                List<titles> element = GetElement(id);
                if (element.Count != 0)
                {
                    titles book = element[0];

                    using (pubs4Entities ctx = new pubs4Entities())
                    {
                        ctx.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
                        for (int i = 0; i < book.titleauthor.Count; i++)
                        {
                            ctx.titleauthor.Attach(book.titleauthor.ElementAt(i));
                            ctx.titleauthor.Remove(book.titleauthor.ElementAt(i));
                        }

                        ctx.titles.Attach(book);
                        ctx.titles.Remove(book);
                        ctx.SaveChanges();
                        deletedElement = book;
                    }
                }
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return deletedElement;


        }

        public virtual void SaveModified(titles book, List<string> authorIds)
        {
            try
            {
                using (pubs4Entities ctx = new pubs4Entities())
                {
                    ctx.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
                    book.type = "UNDECIDED";

                    var list = from a in book.titleauthor
                               select a;

                    List<titleauthor> tas = list.ToList<titleauthor>();

                    for (int i = 0; i < tas.Count; i++ )
                    {
                        ctx.titleauthor.Attach(tas[i]);
                        book.titleauthor.Remove(tas[i]);
                    }


                    for (int i = 0; i < authorIds.Count; i++ )
                    {
                        titleauthor ta = new titleauthor();
                        ta.au_id = authorIds[i];
                        ta.title_id = book.title_id;

                        book.titleauthor.Add(ta);
                    }

                    ctx.publishers.Attach(book.publishers);
                    ctx.titles.Attach(book);
                    ctx.Entry(book).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}