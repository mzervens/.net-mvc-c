﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using md2.Models;
using Nest;

namespace md2.Controllers
{
    public class AuthorController : Controller
    {
        private int pageLimit = 4;
        //
        // GET: /Author/
        public ActionResult Index(int offset)
        {
            AuthorModel authModel = new AuthorMongoModel();
            List<authors> pageData = new List<authors>();
            try
            {
                pageData = authModel.GetPage(offset, pageLimit);
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }
            int nextOffset = offset + pageLimit;
            int prevOffset = offset - pageLimit;

            if (pageData.Count != pageLimit)
            {
                nextOffset = -1;
            }
            if (offset == 0)
            {
                prevOffset = -1;
            }

            ViewData["authorsPage"] = pageData;
            ViewData["offset"] = nextOffset;
            ViewData["prevOffset"] = prevOffset;
            ViewData["action"] = "Index";
            ViewData["controller"] = "Author";

            return View();
        }

        public List<Tweet> getTweetsAboutAuthor(String authorFullName)
        {
            TweetClient tc = new TweetClient();

            var searchResponse = tc.client.Search<Tweet>(s => s
            .Index("events-1")
            .Query(q => q
            .Nested(n => n
            .Path("message")
            .Query(q2 => q2
            .Match(m => m
                .Field(f => f.message.text)
                .Query(authorFullName)
            )
                )
                ))
            );

            List<Tweet> tweets = searchResponse.Documents.ToList<Tweet>();
            tweets.OrderByDescending(d => d.timestamp);
            return tweets;

        }

        public JsonResult FetchAuthors()
        {
            AuthorModel authModel = new AuthorMongoModel();
            List<authors> authors = authModel.GetAllAuthors();

            return Json(authors, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchAuthorsFilter(String from, String to)
        {

            DateTime fr;
            DateTime t;

            try
            {
                fr = DateTime.Parse(from);
            }
            catch(Exception e)
            {
                fr = DateTime.Now;
            }

            try
            {
                t = DateTime.Parse(to);
            }
            catch (Exception e)
            {
                t = DateTime.Now;
            }


            AuthorMongoModel authModel = new AuthorMongoModel();

            List<authors> authors = authModel.GetAuthorsByUpdatedDates(fr, t);

            return Json(authors, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Author/Details/5
        public ActionResult Details(string id)
        {
            AuthorModel authModel = new AuthorMongoModel();
            List<authors> element = new List<authors>();

            try
            {
                element = authModel.GetElement(id);
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            if (element.Count == 1)
            {
                authors auth = element[0];
                String authorFullName = auth.au_fname.Trim() + " " + auth.au_lname.Trim();

                ViewData["author"] = element[0];

                List<Tweet> tweets = getTweetsAboutAuthor(authorFullName);

                ViewBag.tweets = tweets;




                return View("CreateEdit");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public bool validateUserInput(FormCollection collection)
        {
            bool valid = true;

            string authorName = collection.Get("authorName").Trim();
            string authorSurname = collection.Get("authorSurname").Trim();
            string address = collection.Get("address").Trim();

            if (authorName == "" || authorSurname == "" || address == "")
            {
                valid = false;
            }

            return valid;
        }

        //
        // GET: /Author/Create
        public ActionResult Create()
        {
            return View("CreateEdit");
        }

        //
        // POST: /Author/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            string authorName = collection.Get("authorName").Trim();
            string authorSurname = collection.Get("authorSurname").Trim();
            string address = collection.Get("address").Trim();

            bool valid = validateUserInput(collection);

            if (valid)
            {
                string authorId = "";
                AuthorModel authModel = new AuthorMongoModel();
                authors a = new authors();
                a.au_fname = authorName;
                a.au_lname = authorSurname;
                a.address = address;

                try
                {
                    authorId = authModel.CreateElement(a);
                }
                catch (Exception)
                {
                    return RedirectToAction("DbError", "Home");
                }

                return RedirectToAction("Details", "Author", new { id = authorId });
            }
            else
            {
                ViewData["repopulate"] = true;
                ViewData["validationError"] = true;
                ViewData["authorName"] = authorName;
                ViewData["authorSurname"] = authorSurname;
                ViewData["address"] = address;
                return View("CreateEdit");
            }
        }

        //
        // POST: /Author/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            AuthorModel authModel = new AuthorMongoModel();
            authors a = new authors();
            string authorId = collection.Get("authorId");
            string authorName = collection.Get("authorName").Trim();
            string authorSurname = collection.Get("authorSurname").Trim();
            string address = collection.Get("address").Trim();

            try
            {
                List<authors> aList = authModel.GetElement(authorId);
                if (aList.Count == 0)
                {
                    authorId = "";
                }
                else
                {
                    a = aList[0];
                }
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            if (authorId == "")
            {
                return RedirectToAction("Index", "Home");
            }

            bool valid = validateUserInput(collection);


            if (valid)
            {
                a.au_id = authorId;
                a.au_fname = authorName;
                a.au_lname = authorSurname;
                a.address = address;

                try
                {
                    authModel.SaveModified(a);
                }
                catch (Exception)
                {
                    return RedirectToAction("DbError", "Home");
                }

                return RedirectToAction("Details", "Author", new { id = authorId });
            }
            else
            {
                ViewData["repopulate"] = true;
                ViewData["validationError"] = true;
                ViewData["author"] = a;
                return View("CreateEdit");
            }
        }

        //
        // GET: /Author/Delete/5
        public ActionResult Delete(string id)
        {
            AuthorModel authModel = new AuthorMongoModel();
            authors deletedElement = null;
            bool usedInBooks = false;

            try
            {
                deletedElement = authModel.DeleteElement(id);
            }
            catch (InvalidOperationException)
            {
                usedInBooks = true;
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            string status = "";
            string message = "";

            if (usedInBooks)
            {
                status = "warning";
                message = "Šo autoru nevar dzēst, jo tā dati tiek izmantoti sistēmā reģistrētajās grāmatās!";
            }
            else if (deletedElement == null)
            {
                status = "danger";
                message = "Autoru neizdevās izdzēst!";
            }
            else
            {
                status = "success";
                message = "Autors \"" + deletedElement.au_fname + " " + deletedElement.au_lname + "\" veiksmīgi izdzēsts!";
            }

            ViewData["status"] = status;
            ViewData["message"] = message;
            return View("~/Views/Shared/Delete.cshtml");
        }

    }
}
