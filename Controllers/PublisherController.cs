﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using md2.Models;

namespace md2.Controllers
{
    public class PublisherController : Controller
    {
        private int pageLimit = 4;
        //
        // GET: /Publisher/
        public ActionResult Index(int offset)
        {
            PublisherModel pubModel = new PublisherMongoModel();
            List<publishers> pageData = new List<publishers>();
            try
            {
                 pageData = pubModel.GetPage(offset, pageLimit);
            }
            catch(Exception)
            {
                return RedirectToAction("DbError", "Home");
            }
            int nextOffset = offset + pageLimit;
            int prevOffset = offset - pageLimit;

            if(pageData.Count != pageLimit)
            {
                nextOffset = -1;
            }
            if(offset == 0)
            {
                prevOffset = -1;
            }
            
            List<KeyValuePair<string, string>> viewData = new List<KeyValuePair<string,string>>();
            foreach(publishers publisher in pageData)
            {
                KeyValuePair<string, string> publisherIdName = new KeyValuePair<string, string>(publisher._id.ToString(), publisher.pub_name);
                viewData.Add(publisherIdName);
            }

            ViewData["publisherPage"] = viewData;
            ViewData["offset"] = nextOffset;
            ViewData["prevOffset"] = prevOffset;
            ViewData["action"] = "Index";
            ViewData["controller"] = "Publisher";

            return View();
        }

        //
        // GET: /Publisher/Details/5
        public ActionResult Details(string id)
        {

            PublisherModel pubModel = new PublisherMongoModel();
            List<publishers> element = new List<publishers>();

            try
            {
                element = pubModel.GetElement(id);
            }
            catch(Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            if(element.Count == 1)
            {
                ViewData["publisher"] = element[0];
                return View("CreateEdit");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            
        }

        //
        // GET: /Publisher/Create
        public ActionResult Create()
        {
            
            return View("CreateEdit");
        }

        public bool validateUserInput(FormCollection collection)
        {
            bool valid = true;

            string publisherName = collection.Get("publisherName").Trim();
            string publisherCountry = collection.Get("publisherCountry").Trim();
            string publisherCity = collection.Get("publisherCity").Trim();
            string publisherAddress = collection.Get("publisherAddress").Trim();

            if (publisherName == "")
            {
                valid = false;
            }
            else if ((publisherCountry == "" || publisherCity == "") && publisherAddress == "")
            {
                valid = false;
            }

            return valid;
        }

        //
        // POST: /Publisher/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            string publisherName = collection.Get("publisherName").Trim();
            string publisherCountry = collection.Get("publisherCountry").Trim();
            string publisherCity = collection.Get("publisherCity").Trim();
            string publisherAddress = collection.Get("publisherAddress").Trim();

            bool valid = validateUserInput(collection);

            if(valid)
            {
                string publisherId = "";
                PublisherModel pubModel = new PublisherMongoModel();
                publishers p = new publishers();
                p.pub_name = publisherName;
                p.country = publisherCountry;
                p.city = publisherCity;
                p.state = publisherAddress;

                try
                {
                   publisherId = pubModel.CreateElement(p);
                }
                catch(Exception)
                {
                    return RedirectToAction("DbError", "Home");
                }

                return RedirectToAction("Details", "Publisher", new {id = publisherId});
            }
            else
            {
                ViewData["repopulate"] = true;
                ViewData["validationError"] = true;
                ViewData["publisherName"] = publisherName;
                ViewData["publisherCountry"] = publisherCountry;
                ViewData["publisherCity"] = publisherCity;
                ViewData["publisherAddress"] = publisherAddress;
                return View("CreateEdit");
            }

        }


        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            PublisherModel pubModel = new PublisherMongoModel();
            publishers p = new publishers();
            string publisherId = collection.Get("publisherId");
            string publisherName = collection.Get("publisherName").Trim();
            string publisherCountry = collection.Get("publisherCountry").Trim();
            string publisherCity = collection.Get("publisherCity").Trim();
            string publisherAddress = collection.Get("publisherAddress").Trim();

            try
            {
                List<publishers> pList = pubModel.GetElement(publisherId);
                if(pList.Count == 0)
                {
                    publisherId = "";
                }
                else
                {
                    p = pList[0];
                }
            }
            catch(Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            if(publisherId == "")
            {
                return RedirectToAction("Index", "Home");
            }

            bool valid = validateUserInput(collection);


            if (valid)
            {
                p.pub_id = publisherId;
                p.pub_name = publisherName;
                p.country = publisherCountry;
                p.city = publisherCity;
                p.state = publisherAddress;

                try
                {
                    pubModel.SaveModified(p);
                }
                catch (Exception)
                {
                    return RedirectToAction("DbError", "Home");
                }

                return RedirectToAction("Details", "Publisher", new { id = publisherId });
            }
            else
            {
                ViewData["repopulate"] = true;
                ViewData["validationError"] = true;
                ViewData["publisher"] = p;
                return View("CreateEdit");
            }
        }

        //
        // GET: /Publisher/Delete/5
        public ActionResult Delete(string id)
        {
            PublisherModel pubModel = new PublisherMongoModel();
            publishers deletedElement = null;
            bool usedInBooks = false;
            try
            {
                deletedElement = pubModel.DeleteElement(id);
            }
            catch(InvalidOperationException)
            {
                usedInBooks = true;
            }
            catch(Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            string status = "";
            string message = "";

            if (usedInBooks)
            {
                status = "warning";
                message = "Šo izdevniecību nevar dzēst, jo tās dati tiek izmantoti sistēmā reģistrētajās grāmatās!";
            }
            else if(deletedElement == null)
            {
                status = "danger";
                message = "Izdevniecību neizdevās izdzēst!";
            }
            else
            {
                status = "success";
                message = "Izdevniecība \"" + deletedElement.pub_name + "\" veiksmīgi izdzēsta!";
            }

            ViewData["status"] = status;
            ViewData["message"] = message;
            return View("~/Views/Shared/Delete.cshtml");
        }

    }
}
