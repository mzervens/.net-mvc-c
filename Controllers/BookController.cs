﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using md2.Models;
using MongoDB.Bson;

namespace md2.Controllers
{
    public class BookController : Controller
    {
        private int pageLimit = 4;
        //
        // GET: /Book/
        public ActionResult Index(int offset)
        {
            BookModel bookModel = new BookMongoModel();
            List<titles> pageData = new List<titles>();
            try
            {
                pageData = bookModel.GetPage(offset, pageLimit);
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }
            int nextOffset = offset + pageLimit;
            int prevOffset = offset - pageLimit;

            if (pageData.Count != pageLimit)
            {
                nextOffset = -1;
            }
            if (offset == 0)
            {
                prevOffset = -1;
            }

            ViewData["booksPage"] = pageData;
            ViewData["offset"] = nextOffset;
            ViewData["prevOffset"] = prevOffset;
            ViewData["action"] = "Index";
            ViewData["controller"] = "Book";

            return View();
        }

        public JsonResult FetchBooks()
        {
            BookModel bookModel = new BookMongoModel();
            List<titles> books = bookModel.GetAllBooks();

            return Json(books, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchBooksFilter(String from, String to)
        {

            DateTime fr;
            DateTime t;

            try
            {
                fr = DateTime.Parse(from);
            }
            catch (Exception e)
            {
                fr = DateTime.Now;
            }

            try
            {
                t = DateTime.Parse(to);
            }
            catch (Exception e)
            {
                t = DateTime.Now;
            }

            BookMongoModel bookModel = new BookMongoModel();

            List<titles> books = bookModel.GetBooksByUpdatedDates(fr, t);

            return Json(books, JsonRequestBehavior.AllowGet);
        }

        public List<Tweet> getTweetsAboutBook(String bookTitle)
        {
            TweetClient tc = new TweetClient();

            var searchResponse = tc.client.Search<Tweet>(s => s
            .Index("events-1")
            .Query(q => q
            .Nested(n => n
            .Path("message")
            .Query(q2 => q2
            
            .MatchPhrase(m => m
                .Field(f => f.message.text)
                .Strict(true)
                .Query(bookTitle)
            )
                )
                ))
            );

            List<Tweet> tweets = searchResponse.Documents.ToList<Tweet>();
            tweets.OrderByDescending(d => d.timestamp);
            return tweets;

        }

        //
        // GET: /Book/Details/5
        public ActionResult Details(string id)
        {
            BookModel bookModel = new BookMongoModel();
            AuthorModel authModel = new AuthorMongoModel();
            PublisherModel pubModel = new PublisherMongoModel();
            List<titles> element = new List<titles>();
            List<publishers> pubs = new List<publishers>();
            List<authors> auths = new List<authors>();

            try
            {
                element = bookModel.GetElement(id);
                pubs = pubModel.GetAllPublishers();
                auths = authModel.GetAllAuthors();
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            if (element.Count == 1)
            {
                titles title = element[0];
                List<Tweet> tweets = getTweetsAboutBook(title.title.Trim());

                ViewData["bookTitle"] = element[0];
                ViewData["publishers"] = pubs;
                ViewData["allAuthors"] = auths;
                ViewBag.tweets = tweets;
                return View("CreateEdit");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //
        // GET: /Book/Create
        public ActionResult Create()
        {
            AuthorModel authModel = new AuthorMongoModel();
            PublisherModel pubModel = new PublisherMongoModel();
            List<authors> authors = null;
            List<publishers> pubs = new List<publishers>();

            try
            {
                authors = authModel.GetAllAuthors();
                pubs = pubModel.GetAllPublishers();
            }
            catch(Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            ViewData["allAuthors"] = authors;
            ViewData["publishers"] = pubs;
            return View("CreateEdit");
        }

        public ActionResult GetAuthorPartial(int id)
        {
            AuthorModel authModel = new AuthorMongoModel();
            List<authors> authors = null;
            authors = authModel.GetAllAuthors();
            ViewData["data"] = authors;
            ViewData["id"] = id;
            ViewData["selectedIndx"] = "";
            return PartialView("AuthorEntity");
        }

        public List<string> getUniqueAuthors(FormCollection collection)
        {
            Dictionary<string, string> uniqueAuthors = new Dictionary<string, string>();
            List<string> output = new List<string>();
            for (int i = 0; i < collection.Count; i++)
            {
                string key = collection.GetKey(i);
                if(key.Contains("author-"))
                {
                    //Autors
                    string val = collection.Get(key);
                    if (val == "Autors" || val == "")
                    {
                        continue;
                    }
                    else if (uniqueAuthors.ContainsKey(val))
                    {
                        continue;
                    }
                    else
                    {
                        uniqueAuthors.Add(val, val);
                        output.Add(val);
                    }
                }
            }

            return output;
        }

        //
        // POST: /Book/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            PublisherModel pubModel = new PublisherMongoModel();
            AuthorModel authModel = new AuthorMongoModel();
            List<publishers> pubs = new List<publishers>();
            List<authors> auths = new List<authors>();

            try
            {
                pubs = pubModel.GetAllPublishers();
                auths = authModel.GetAllAuthors();
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            string titleName = collection.Get("titleName").Trim();
            string titleDate = collection.Get("titleDate").Trim();
            string titleYear = collection.Get("titleYear").Trim();
            string titlePublisher = collection.Get("publisher");
            List<string> authorIds = getUniqueAuthors(collection);
            System.Diagnostics.Debug.WriteLine("fuken pubs " + titlePublisher);
            bool valid = true;

            if (titleName == "" || (titleDate == "" && titleYear == "") || authorIds.Count == 0 || titlePublisher == "Izdevniecība" || titlePublisher == "")
            {
                valid = false;
            }

            if (valid)
            {
                string titleId = "";
                if(titleYear == "")
                {
                    titleYear = "-1";
                }
                BookModel bookModel = new BookMongoModel();
                titles t = new titles();
                t.title = titleName;
                t.pubdate = Convert.ToDateTime(titleDate);
                t.royalty = int.Parse(titleYear);
                publishers pub = new publishers {pub_id = titlePublisher, _id = ObjectId.Parse(titlePublisher)};
                t.publishers = pub;

                try
                {
                    titleId = bookModel.CreateElement(t, authorIds);
                }
                catch (Exception)
                {
                    return RedirectToAction("DbError", "Home");
                }

                return RedirectToAction("Details", "Book", new { id = titleId });
            }
            else
            {
                ViewData["repopulate"] = true;
                ViewData["validationError"] = true;
                ViewData["titleName"] = titleName;
                ViewData["titleDate"] = titleDate;
                ViewData["titleYear"] = titleYear;
                ViewData["selectedPub"] = titlePublisher;
                ViewData["publishers"] = pubs;
                ViewData["allAuthors"] = auths;
                return View("CreateEdit");
            }
        }

        //
        // POST: /Book/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            PublisherModel pubModel = new PublisherMongoModel();
            AuthorModel authModel = new AuthorMongoModel();
            BookModel bookModel = new BookMongoModel();
            titles t = new titles();

            List<publishers> pubs = new List<publishers>();
            List<authors> auths = new List<authors>();

            try
            {
                pubs = pubModel.GetAllPublishers();
                auths = authModel.GetAllAuthors();
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            string titleName = collection.Get("titleName").Trim();
            string titleDate = collection.Get("titleDate").Trim();
            string titleYear = collection.Get("titleYear").Trim();
            string titlePublisher = collection.Get("publisher");
            string titleId = collection.Get("titleId");
            List<string> authorIds = getUniqueAuthors(collection);

            try
            {
                List<titles> tList = bookModel.GetElement(titleId);
                if (tList.Count == 0)
                {
                    titleId = "";
                }
                else
                {
                    t = tList[0];
                }
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            if (titleId == "")
            {
                return RedirectToAction("Index", "Home");
            }

            bool valid = true;

            if (titleName == "" || (titleDate == "" && titleYear == "") || authorIds.Count == 0 || titlePublisher == "Izdevniecība" || titlePublisher == "")
            {
                valid = false;
            }


            if (valid)
            {
                if (titleYear == "")
                {
                    titleYear = "-1";
                }

                t.title = titleName;
                t.pubdate = Convert.ToDateTime(titleDate);
                t.royalty = int.Parse(titleYear);
                publishers pub = new publishers { pub_id = titlePublisher, _id = ObjectId.Parse(titlePublisher) };
                t.pub_id = titlePublisher;
                t.publishers = pub;

                try
                {
                    bookModel.SaveModified(t, authorIds);
                }
                catch (Exception)
                {
                    return RedirectToAction("DbError", "Home");
                }

                return RedirectToAction("Details", "Book", new { id = titleId });
            }
            else
            {
                ViewData["repopulate"] = true;
                ViewData["validationError"] = true;
                ViewData["titleName"] = titleName;
                ViewData["titleDate"] = titleDate;
                ViewData["titleYear"] = titleYear;
                ViewData["selectedPub"] = titlePublisher;
                ViewData["publishers"] = pubs;
                ViewData["allAuthors"] = auths;
                return View("CreateEdit");
            }
        }

        //
        // GET: /Book/Delete/5
        public ActionResult Delete(string id)
        {
            BookModel bookModel = new BookMongoModel();
            titles deletedElement = null;
            
            try
            {
                deletedElement = bookModel.DeleteElement(id);
            }
            catch (Exception)
            {
                return RedirectToAction("DbError", "Home");
            }

            string status = "";
            string message = "";

            if (deletedElement == null)
            {
                status = "danger";
                message = "Grāmatu neizdevās izdzēst!";
            }
            else
            {
                status = "success";
                message = "Grāmata \"" + deletedElement.title + "\" veiksmīgi izdzēsta!";
            }

            ViewData["status"] = status;
            ViewData["message"] = message;
            return View("~/Views/Shared/Delete.cshtml");
        }

    }
}
