﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using md2.Models;
using MongoDB.Driver;
using MongoDB.Bson;

namespace md2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            
            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> MigrateToMongoDB()
        {
            MongoClientClass cc = new MongoClientClass("pubs4");
            PublisherModel pm = new PublisherModel();
            AuthorModel am = new AuthorModel();
            BookModel bm = new BookModel();

            

            List<publishers> publishersL = pm.GetAllPublishers();
            List<authors> authorL = am.GetAllAuthors();
            List<titles> titlesL = bm.GetAllBooks();

            var collection = cc.mDB.GetCollection<BsonDocument>("publishers");
            var collectionAuthors = cc.mDB.GetCollection<BsonDocument>("authors");
            var collectionTitles = cc.mDB.GetCollection<BsonDocument>("titles");
            
            // migrates publishers, authors and titles to mongodb
            

            foreach (publishers p in publishersL)
            {

                
                BsonDocument pubDoc = new BsonDocument();
                pubDoc.Add(new BsonElement("pub_name", p.pub_name));
                pubDoc.Add(new BsonElement("city", p.city));
                pubDoc.Add(new BsonElement("state", (BsonValue)p.state ?? BsonNull.Value));
                pubDoc.Add(new BsonElement("country", p.country));


                collection.InsertOne(pubDoc);

            }
           

            

            foreach (authors a in authorL)
            {

                BsonDocument authDoc = new BsonDocument();
                authDoc.Add(new BsonElement("au_lname", a.au_lname));
                authDoc.Add(new BsonElement("au_fname", a.au_fname));
                authDoc.Add(new BsonElement("phone", a.phone));
                authDoc.Add(new BsonElement("address", a.address));
                authDoc.Add(new BsonElement("city", a.city));
                authDoc.Add(new BsonElement("state", a.state));
                authDoc.Add(new BsonElement("zip", a.zip));
                authDoc.Add(new BsonElement("contract", a.contract));

                collectionAuthors.InsertOne(authDoc);

            }
             

            

            int k = 6;


            foreach (titles t in titlesL)
            {

                BsonDocument titleDoc = new BsonDocument();
                titleDoc.Add(new BsonElement("title", t.title));
                titleDoc.Add(new BsonElement("type", t.type));
                titleDoc.Add(new BsonElement("price", (BsonValue)t.price ?? BsonNull.Value));
                titleDoc.Add(new BsonElement("advance", (BsonValue)t.advance ?? BsonNull.Value));
                titleDoc.Add(new BsonElement("royalty", (BsonValue)t.royalty ?? BsonNull.Value));
                titleDoc.Add(new BsonElement("ytd_sales", (BsonValue)t.ytd_sales ?? BsonNull.Value));
                titleDoc.Add(new BsonElement("notes", (BsonValue)t.notes ?? BsonNull.Value));
                titleDoc.Add(new BsonElement("pubdate", t.pubdate));

                collectionTitles.InsertOne(titleDoc);

            }
            //
            

            // adds publisher references to titles
            
            foreach (titles t in titlesL)
            {
                var filter = new BsonDocument("title", t.title);
                BsonDocument pub = new BsonDocument();
                var pubFilter = new BsonDocument("pub_name", t.publishers.pub_name);


                await collection.Find(pubFilter)
                        .ForEachAsync(pubDoc => { pub = pubDoc; });

                var update = Builders<BsonDocument>.Update
                    .Set("publisher", pub.GetValue("_id"));

                await collectionTitles.UpdateManyAsync(filter, update);
            }
            //
            


            // adds author references to titles
            // will migrate incorrectly those title authors where titles are identical
            
            foreach (titles t in titlesL)
            {

                List<titleauthor> authors = t.titleauthor.ToList<titleauthor>();
                BsonArray titleAuths = new BsonArray();
                var titleFilter = new BsonDocument("title", t.title);

                foreach (titleauthor ta in authors)
                {
                    authors aut = am.GetElement(ta.au_id)[0];

                    var filter = new BsonDocument("au_lname", aut.au_lname);
                    filter.Add("au_fname", aut.au_fname);

                    await collectionAuthors.Find(filter)
                            .ForEachAsync(auth => { var authId = auth.GetValue("_id");
                                                    titleAuths.Add(authId);

                            });
                }

                var update = Builders<BsonDocument>.Update
                                .Set("authors", titleAuths);

                await collectionTitles.UpdateManyAsync(titleFilter, update);

            }
            
            //


            return Content("Success!");

        }

        public ActionResult DbError()
        {
            return View();
        }

        public ActionResult Apraksts2()
        {
            return View();
        }

        public ActionResult GetReportAboutBooks()
        {
            MongoClientClass cc = new MongoClientClass("pubs4");
            var titleCol = cc.mDB.GetCollection<titles>("titles");
            var publisherCol = cc.mDB.GetCollection<publishers>("publishers");

            var aggregate = titleCol.Aggregate().Group(new BsonDocument { { "_id", "$publisher" }, { "count", new BsonDocument("$sum", 1) } }).ToList<BsonDocument>();

            foreach(BsonDocument aggr in aggregate)
            {
                var pub = publisherCol.Find(new BsonDocument("_id", aggr["_id"])).ToList<publishers>()[0];
                aggr.Add("pub_name", pub.pub_name);
            }

            ViewData["publisherBookInfo"] = aggregate;

            return View();
        }

    }
}