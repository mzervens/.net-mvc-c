﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace md2
{
    public class Rand
    {
        private string data = "qwertyuiopasdfghjklzxcvbnm1234567890";
        private string dataNumbers = "1234567890";
        private Random r = new Random();

        public string GetRandomString(int length, bool onlyNumbers = false)
        {
            string dta = data;
            if(onlyNumbers)
            {
                dta = dataNumbers;
            }
            string result = "";
            for (int i = 0; i < length; i++)
            {
                int index = r.Next(0, dta.Length - 1);
                result += dta[index];
            }

            return result;
        }

    }
}