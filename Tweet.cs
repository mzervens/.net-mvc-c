﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace md2
{
    public class Message
    {
        public String text;
        public int retweet_count;
    }

    public class Author
    {
        public String screen_name;
        public String image;
        public String id;
    }

    [Nest.ElasticsearchType(Name = "tweets")]
    public class Tweet
    {
        public String _id;
        public Message message;
        public Author author;
        public DateTime timestamp;
    }
}