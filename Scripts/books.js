﻿$(function ()
{
    var counter = getSetAuthorEntries() + 1;

    enableSelectPickers();

    $('#titleDate').datepicker({
        format: 'yyyy.mm.dd',
    })

    $("#addAuthor").on("click", function (event)
    {
        event.preventDefault();
        console.log("add new author");
        getAuthorEntry(counter);

    });

    $("#authorContainer").on('click', '.removeAuthor', function(event) 
    {
        console.log("here");
        event.preventDefault();

        if (getSetAuthorEntries() <= 1)
        {
            return;
        }

        var clickedEle = $(this);
        var parent = clickedEle.closest(".auth");
        parent.remove();
        console.log("remove author entry");
    });

    function enableSelectPickers()
    {
        $(".authorSelect").selectpicker();
        $(".selectpicker").selectpicker();
    }

    function getSetAuthorEntries()
    {
        return $("#authorContainer .form-group").length;
    }

    function getAuthorEntry(id)
    {
        $.ajax
       ({
           type: "get",
           url: "/Book/GetAuthorPartial/" + id,
           success: function(data)
           {
               $("#authorContainer").prepend(data);
               enableSelectPickers();
               counter++;
           }

        });

    }

});