/// <autosync enabled="true" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="bootstrap.js" />
/// <reference path="respond.js" />
/// <reference path="publishers.js" />
/// <reference path="books.js" />
/// <reference path="bootstrap-select.js" />
/// <reference path="bootstrap-datepicker.js" />
/// <reference path="i18n/defaults-bg_bg.js" />
/// <reference path="i18n/defaults-cs_cz.js" />
/// <reference path="i18n/defaults-da_dk.js" />
/// <reference path="i18n/defaults-de_de.js" />
/// <reference path="i18n/defaults-en_us.js" />
/// <reference path="i18n/defaults-es_cl.js" />
/// <reference path="i18n/defaults-eu.js" />
/// <reference path="i18n/defaults-fa_ir.js" />
/// <reference path="i18n/defaults-fr_fr.js" />
/// <reference path="i18n/defaults-hu_hu.js" />
/// <reference path="i18n/defaults-it_it.js" />
/// <reference path="i18n/defaults-ko_kr.js" />
/// <reference path="i18n/defaults-nl_nl.js" />
/// <reference path="i18n/defaults-pl_pl.js" />
/// <reference path="i18n/defaults-pt_br.js" />
/// <reference path="i18n/defaults-pt_pt.js" />
/// <reference path="i18n/defaults-ro_ro.js" />
/// <reference path="i18n/defaults-ru_ru.js" />
/// <reference path="i18n/defaults-sk_sk.js" />
/// <reference path="i18n/defaults-sl_si.js" />
/// <reference path="i18n/defaults-sv_se.js" />
/// <reference path="i18n/defaults-tr_tr.js" />
/// <reference path="i18n/defaults-ua_ua.js" />
/// <reference path="i18n/defaults-zh_cn.js" />
/// <reference path="i18n/defaults-zh_tw.js" />
/// <reference path="locales/bootstrap-datepicker.ar.js" />
/// <reference path="locales/bootstrap-datepicker.az.js" />
/// <reference path="locales/bootstrap-datepicker.bg.js" />
/// <reference path="locales/bootstrap-datepicker.bs.js" />
/// <reference path="locales/bootstrap-datepicker.ca.js" />
/// <reference path="locales/bootstrap-datepicker.cy.js" />
/// <reference path="locales/bootstrap-datepicker.cs.js" />
/// <reference path="locales/bootstrap-datepicker.da.js" />
/// <reference path="locales/bootstrap-datepicker.de.js" />
/// <reference path="locales/bootstrap-datepicker.el.js" />
/// <reference path="locales/bootstrap-datepicker.en-gb.js" />
/// <reference path="locales/bootstrap-datepicker.es.js" />
/// <reference path="locales/bootstrap-datepicker.et.js" />
/// <reference path="locales/bootstrap-datepicker.eu.js" />
/// <reference path="locales/bootstrap-datepicker.fa.js" />
/// <reference path="locales/bootstrap-datepicker.fi.js" />
/// <reference path="locales/bootstrap-datepicker.fo.js" />
/// <reference path="locales/bootstrap-datepicker.fr-ch.js" />
/// <reference path="locales/bootstrap-datepicker.fr.js" />
/// <reference path="locales/bootstrap-datepicker.gl.js" />
/// <reference path="locales/bootstrap-datepicker.he.js" />
/// <reference path="locales/bootstrap-datepicker.hy.js" />
/// <reference path="locales/bootstrap-datepicker.hr.js" />
/// <reference path="locales/bootstrap-datepicker.hu.js" />
/// <reference path="locales/bootstrap-datepicker.id.js" />
/// <reference path="locales/bootstrap-datepicker.is.js" />
/// <reference path="locales/bootstrap-datepicker.it-ch.js" />
/// <reference path="locales/bootstrap-datepicker.it.js" />
/// <reference path="locales/bootstrap-datepicker.ja.js" />
/// <reference path="locales/bootstrap-datepicker.ka.js" />
/// <reference path="locales/bootstrap-datepicker.kh.js" />
/// <reference path="locales/bootstrap-datepicker.kk.js" />
/// <reference path="locales/bootstrap-datepicker.kr.js" />
/// <reference path="locales/bootstrap-datepicker.lt.js" />
/// <reference path="locales/bootstrap-datepicker.lv.js" />
/// <reference path="locales/bootstrap-datepicker.me.js" />
/// <reference path="locales/bootstrap-datepicker.mk.js" />
/// <reference path="locales/bootstrap-datepicker.ms.js" />
/// <reference path="locales/bootstrap-datepicker.nb.js" />
/// <reference path="locales/bootstrap-datepicker.nl-be.js" />
/// <reference path="locales/bootstrap-datepicker.nl.js" />
/// <reference path="locales/bootstrap-datepicker.no.js" />
/// <reference path="locales/bootstrap-datepicker.pl.js" />
/// <reference path="locales/bootstrap-datepicker.pt-br.js" />
/// <reference path="locales/bootstrap-datepicker.pt.js" />
/// <reference path="locales/bootstrap-datepicker.ro.js" />
/// <reference path="locales/bootstrap-datepicker.rs-latin.js" />
/// <reference path="locales/bootstrap-datepicker.rs.js" />
/// <reference path="locales/bootstrap-datepicker.ru.js" />
/// <reference path="locales/bootstrap-datepicker.sk.js" />
/// <reference path="locales/bootstrap-datepicker.sl.js" />
/// <reference path="locales/bootstrap-datepicker.sq.js" />
/// <reference path="locales/bootstrap-datepicker.sr-latin.js" />
/// <reference path="locales/bootstrap-datepicker.sr.js" />
/// <reference path="locales/bootstrap-datepicker.sv.js" />
/// <reference path="locales/bootstrap-datepicker.sw.js" />
/// <reference path="locales/bootstrap-datepicker.th.js" />
/// <reference path="locales/bootstrap-datepicker.tr.js" />
/// <reference path="locales/bootstrap-datepicker.uk.js" />
/// <reference path="locales/bootstrap-datepicker.vi.js" />
/// <reference path="locales/bootstrap-datepicker.zh-tw.js" />
